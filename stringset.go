/*
Copyright (c) 2014, Thomas Lingefelt <thomasrling@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/*
StringSet - Maintaining a unique set of strings

Example
	ss := stringset.New()
	ss.Add("This", "is", "a", "test")
	if ss.Has("test") {
		fmt.Println("TESTING!")
	}
*/
package stringset

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"sync"
)

type nothing struct {
}

var nope nothing

type StringSet struct {
	m map[string]nothing
	sync.RWMutex
}

func FromSlice(strs []string) *StringSet {
	ss := New()
	ss.Add(strs...)
	return ss
}

func New() *StringSet {
	return &StringSet{m: make(map[string]nothing)}
}

// Return values in set as a slice
func (ss *StringSet) Slice() []string {
	ss.RLock()
	defer ss.RUnlock()
	slice := make([]string, len(ss.m), len(ss.m))
	i := 0
	for k, _ := range ss.m {
		slice[i] = k
		i++
	}
	return slice
}

// Check if a string is in the set.
func (ss *StringSet) Has(str string) bool {
	ss.RLock()
	defer ss.RUnlock()
	_, ok := ss.m[str]
	return ok
}

// Add strings to the set.
func (ss *StringSet) Add(vs ...string) {
	ss.Lock()
	defer ss.Unlock()
	for _, v := range vs {
		ss.m[v] = nope
	}
}

// Delete strings from the set
func (ss *StringSet) Del(vs ...string) {
	ss.Lock()
	defer ss.Unlock()
	for _, v := range vs {
		delete(ss.m, v)
	}
}

func (ss *StringSet) Len() int {
	return len(ss.m)
}

func (ss *StringSet) MarshalJSON() ([]byte, error) {
	return json.Marshal(ss.Slice())
}

func (ss *StringSet) UnmarshalJSON(jsonBlob []byte) error {
	var slice []string
	err := json.Unmarshal(jsonBlob, &slice)
	if err != nil {
		return err
	}
	ss.m = make(map[string]nothing)
	ss.Add(slice...)
	return nil
}

func (ss *StringSet) MarshalBinary() ([]byte, error) {
	var buff bytes.Buffer
	gEnc := gob.NewEncoder(&buff)
	err := gEnc.Encode(ss.Slice())
	return buff.Bytes(), err
}

func (ss *StringSet) UnmarshalBinary(gobBlob []byte) error {
	buff := bytes.NewBuffer(gobBlob)
	gDec := gob.NewDecoder(buff)
	var slice []string
	err := gDec.Decode(&slice)
	if err != nil {
		return err
	}
	ss.m = make(map[string]nothing)
	ss.Add(slice...)
	return nil
}
